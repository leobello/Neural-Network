import torch
import datetime
import PIL.Image as Image
import numpy as np
import urllib2
from torchvision import transforms


def identify(imgPath, model, item_list):
    minimalRate = 0
    startingTime = datetime.datetime.now()
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    tr = transforms.Compose([
        transforms.Resize(256),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ])

    img_tensor = tr(Image.open(urllib2.urlopen(imgPath))).unsqueeze_(0).to(device)
    model.to(device)

    output = model(img_tensor)
    print(output)
    if(np.max(output[0].cpu().detach().numpy()) < minimalRate):
        return("Je ne sais pas ce que c'est")
    else:
	executionTime = datetime.datetime.now()-startingTime
        return("C'est un " + item_list[np.argmax(output[0].cpu().detach().numpy())])



