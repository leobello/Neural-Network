#!/usr/bin/env python
import torch
import rospy
from naoqi import *
from std_msgs.msg import String
from PepperVision2 import identify

class Recognition():
	def __init__(self,name,networkName,imgClassList,treshold):
		# Loading the neural network
                self.model_conv = torch.load(networkName,map_location='cpu')
                self.model_conv.eval()
		# Define images class
		self.list = imgClassList
		# Define treshold
		self.treshold = treshold

		# Initializing tablet and speaker modules1
		self.tabletService = ALProxy("ALTabletService")
		self.tabletService.resetTablet()
		self.speaker = ALProxy("ALTextToSpeech")

	def recognize(self,data):
        	rospy.loginfo(identify(data.data, self.model_conv, self.list,self.treshold)+data.data)
		self.speaker.say(identify(data.data, self.model_conv, self.list))
		# Display the photo on the tablet
		self.tabletService.showImageNoCache(data.data)

def recognition_listener():
        rospy.init_node('receiver',anonymous=False)
        # Subscribing to the channel where images path are sended
	rospy.Subscriber("imgPathChan", String, pythonRecognitionModule.recognize)
        # Loop
	rospy.spin()


if __name__ == '__main__':
	try:
        	global broker
        	with open(rospkg.RosPack().get_path("speak")+"/../config/config.cfg", "r") as gFile:
            		content = gFile.readlines()
            		broker = ALBroker("pythonBroker", "0.0.0.0", 0, content[0].strip(), int(content[1].strip()))
		global pythonRecognitionModule
		pythonRecognitionModule = Recognition("pythonRecognitionModule","PepperNet",['bouteille','chaise','verre'],treshold)
		recognition_listener()
	except rospy.ROSInterruptException:
		pass


