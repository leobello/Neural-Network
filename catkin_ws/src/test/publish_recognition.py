#!/usr/bin/env python
import rospy
import torch
import vision_definitions
from naoqi import *
from std_msgs.msg import String

def captureAndIdentify():
	camera = ALProxy("ALPhotoCapture")
 	life = ALProxy("ALAutonomousLife")

	# k4VGA = 1280 * 960 px
	camera.setResolution(vision_definitions.k4VGA)
	camera.setColorSpace(vision_definitions.kYUVColorSpace)
	camera.setPictureFormat("jpg")

	# Pepper shouldn't move ( So we disable all autonomous ability )
	life.setAutonomousAbilityEnabled("All",False)
	camera.takePicture("/home/nao/.local/share/PackageManager/apps/test_camera/html/", "imageTest")

	# Publishing the image for the subscriber
	pub = rospy.Publisher('imgPathChan', String, queue_size=20)
	rospy.init_node('sender')
	pub.publish("http://192.168.0.100/apps/test_camera/imageTest.jpg")

	# Pepper can move now
	life.setAutonomousAbilityEnabled("All",True)

if __name__ = '__main__':
	try:
		with open(rospkg.RosPack().get_path("speak")+"/../config/config.cfg", "r") as gFile:
			content = gFile.readlines()
			broker = ALBroker("pythonBroker", "0.0.0.0", 0, content[0].strip(), int(content[1].strip()))
			captureAndIdentify()
		except rospy.ROSInterruptException:
			pass
