#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/stehlinq/catkin_ws/devel:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/stehlinq/catkin_ws/devel/lib:$LD_LIBRARY_PATH"
export PATH="/opt/ros/lunar/bin:/home/stehlinq/bin:/home/stehlinq/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/mnt/c/ProgramData/Oracle/Java/javapath_target_2410137000:/mnt/c/Program Files (x86)/Intel/iCLS Client:/mnt/c/Program Files/Intel/iCLS Client:/mnt/c/Windows/System32:/mnt/c/Windows:/mnt/c/Windows/System32/wbem:/mnt/c/Windows/System32/WindowsPowerShell/v1.0:/mnt/c/Program Files/Intel/WiFi/bin:/mnt/c/Program Files/Common Files/Intel/WirelessCommon:/mnt/c/Program Files (x86)/Intel/Intel(R) Management Engine Components/DAL:/mnt/c/Program Files/Intel/Intel(R) Management Engine Components/DAL:/mnt/c/Program Files (x86)/Intel/Intel(R) Management Engine Components/IPT:/mnt/c/Program Files/Intel/Intel(R) Management Engine Components/IPT:/mnt/c/Program Files (x86)/NVIDIA Corporation/PhysX/Common:/mnt/c/Program Files (x86)/GtkSharp/2.12/bin:/mnt/c/Windows/System32:/mnt/c/Windows:/mnt/c/Windows/System32/wbem:/mnt/c/Windows/System32/WindowsPowerShell/v1.0:/mnt/c/Program Files/Git/cmd:/mnt/c/Users/Tsuzu/AppData/Local/Microsoft/WindowsApps:/mnt/c/Users/Tsuzu/AppData/Local/GitHubDesktop/bin:/mnt/c/Users/Tsuzu/AppData/Local/atom/bin:/snap/bin"
export PWD="/home/stehlinq/catkin_ws/build"
export ROSLISP_PACKAGE_DIRECTORIES="/home/stehlinq/catkin_ws/devel/share/common-lisp"
export ROS_PACKAGE_PATH="/home/stehlinq/catkin_ws/src:$ROS_PACKAGE_PATH"